package processing;

import java.sql.Connection;

public class Main {
	
	 
	static int lnnumber;
	static Connection conn;
	static int IdNumMax;
	static int IdNum;
	static int[][] IdArr;
	
	
	
//	static String []result;
	
	public static void main(String[] args) throws Exception {
		
		
		Input csv = new Input();
		lnnumber = csv.count("/home/zkny/eclipse-workspace/ReadFile/src/teszt.csv");		//input sorainak szama
		String[][] lines = new String[lnnumber][11];										//tomb az input tarolasara
		lines = csv.fRead("/home/zkny/eclipse-workspace/ReadFile/src/teszt.csv");			
//		System.out.println(Arrays.deepToString(lines));
		
		//adatbazishoz csatlakozunk
		DataBase DB = new DataBase();
		conn = DB.Connect();

		//lines sorain vegig megyunk for looppal
		//checkoljuk a formatumokat, elkeszitjuk a response filet(felig)
		String[][] respLines = new String[lnnumber][3];				//headers
		respLines[0][0]= "LineNumber";
		respLines[0][1]= "Status";
		respLines[0][2]= "Message";
		
		//fejlec sora miatt 1-tol kezdjuk
		for (int i = 1; i < respLines.length; i++) {
			respLines[i]= csv.validate(lines[i], respLines[i] );
//			System.out.println("lines[i])" + Arrays.toString(lines[i])+ "respLines[i]" +Arrays.toString(respLines[i]));
		}
		
		IdNum = DB.DbLines(conn);
		IdNumMax = IdNum + lines.length;
		int[] OrderIdArr = new int[IdNumMax];		//adatbazisban tartol idk szama a tomb ini
		OrderIdArr = DB.getOrderIds(conn, IdNumMax);
		int[] OrderItemIdArr = new int[IdNumMax];		//adatbazisban tartol idk szama a tomb ini
		OrderItemIdArr = DB.getOrderItemIds(conn, IdNumMax);
		
		for (int i = 1; i<lines.length; i++) {
			//Orderid check
//			System.out.println("lines["+i+"] = "+lines[i][2]);
			boolean duplicate = false;
			for (int a = 0; a < IdNumMax; a++) {
				if (Integer.parseInt(lines[i][2]) == OrderIdArr[a]) {
					respLines[i][2]= respLines[i][2] + "OrderId already in use";
					duplicate = true;
					break;
				}
			}
				respLines[i][0]= lines[i][0];
				if (IdNum < IdNumMax && duplicate == false) {
//					System.out.println("New unique ID");
					OrderIdArr[IdNum] = Integer.parseInt(lines[i][2]) ;
					IdNum++;
				}
			
		}
		

		IdNum = DB.DbLines(conn);
		for (int i = 1; i<lines.length; i++) {
		//Orderid check
		boolean duplicate = false;
		for (int a = 0; a < IdNumMax; a++) {
			if (Integer.parseInt(lines[i][2]) == OrderItemIdArr[a]) {
				respLines[i][2]= respLines[i][2] + "OrderItemId already in use";
				duplicate = true;
				break;
			}
		}
			respLines[i][0]= lines[i][0];
			if (IdNum < IdNumMax && duplicate == false) {
				OrderIdArr[IdNum] = Integer.parseInt(lines[i][2]) ;
				IdNum++;
			}
	}
		
		for (int i = 1; i < respLines.length; i++) {
			if (respLines[i][2].isEmpty()==true) {
				DB.update(conn, lines[i], respLines[i]);
				
			}
		}
		
		//responseFile elkeszitese
			csv.out(respLines);
			
//		
//		//csv file tartalmat a lines arraylistbe toltjuk mert nem tudjuk a hosszat elore
//		ArrayList fileContent = new ArrayList();
//		ReadCSV reader = new ReadCSV();
//		fileContent = reader.fRead("/home/zkny/eclipse-workspace/ReadFile/src/teszt.csv");
////		for(int i = 0; i < fileContent.size(); i++) {
////			Object row = fileContent.get(i);
////			System.out.println(row.toString());
////		}
//		
//		//adatbazisban keressuk az ismetlodeseket
//		connection.getIds(connection.getConnection());
//		
//		
		
	}

}
