package processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Input {
	
//	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	
	//inputFile soarait tartalmazo tomb mainben valo definialasahoz visszaadjuk a sorok szamat
	public static int count (String path) throws IOException{
		BufferedReader reader = null;
		{
			try {
			    File file = new File(path);
			    reader = new BufferedReader(new FileReader(file));
			    int ln = 0;
			    String line;
				while ((line = reader.readLine()) != null) {
//			    	String[] asd = line.split(";");
//			    	 System.out.println("asd: " + Arrays.toString(asd));
			        ln++;
			    }
			    reader.close();
		return ln;
				}
			finally {
				}
			}
	}
	
	//inputFile sorait adja vissza
	public static String[][] fRead(String path) {

		
		BufferedReader reader = null;		//a sorok szamanak visszaadasahoz
		BufferedReader reader2 = null;		//lines tomb feltoltesehez
		{
			
			try {
			    File file = new File(path);
			    reader = new BufferedReader(new FileReader(file));
			    reader2 = new BufferedReader(new FileReader(file));
			    String line;
			    int ln = 0;
			    String[] asd = null;
				// count how many lines we have
			    while ((line = reader.readLine()) != null) {
			    	asd = line.split(";");
			        ln++;
			    }
			    //init the lines variable, and loop trough again the rows
			    String[][] lines = new String[ln][12];
			    int i = 0;
			    while ((line = reader2.readLine()) != null) {
			    	asd = line.split(";");
				    	for (int a = 0; a < 12; a++) {
				    		if (a > asd.length-1) {
				    			 LocalDate localDate = LocalDate.now();
				    			lines[i][a] = DateTimeFormatter.ofPattern("yyy-MM-dd").format(localDate).toString();
				    		} else {
				    			lines[i][a] = asd[a];
				    		}
				    	}
				    	i++;
			    }
			    reader2.close();
			    return lines;
			    
			} catch (IOException e) {
			    e.printStackTrace();
			} finally {
			    try {
			        reader.close();
			    } 
			    catch (IOException e) {
			        e.printStackTrace();
			    }
			} 

		}
		return null;
		

}


	public static String isInteger (String str)
	{  
		
		 try  
		  {  
			//Integer result = Integer.valueOf(str);	
		    int d = Integer.parseInt(str);
		    
		    return "OK";
		  }  
		  catch(NumberFormatException nfe)  
		  {  
			  
			  return nfe.toString(); 
		  }  
	    
	}
	
	public static String Empty (String str) {
		if(str == "") {
			return "OK";
		}
		else {
			return "Empty string!";
		}
		}
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	public static String isEmail (String str) {
		        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(str);
		        if (matcher.find()==true) {
		        	return "OK";
		        }
				return "Invalid email format";
		}
	
	public static String isValidPrice (String str, double i) {
		if (isDouble(str) == "OK") {
			if (Double.parseDouble(str) >= i) {
				return "OK";
			}
			else {
				return "Less than minimum price";
			}
		}
		else {
			return "Not double";
		}
	}
	
	public static String isDouble (String str)
	{  
	  try  
	  {  
	    Double d = Double.parseDouble(str);
	    
	    return "OK";
	  }  
	  catch(NumberFormatException nfe)  
	  {  
		  
		  return nfe.toString(); 
	  }  
	}

	public static String isStatus (String str) {
		if (str == "IN_STOCK" || str == "OUT_OF_STOCK") {
			return "OK";
		}
		else {
			return "Invalid status";
		}
	}
	
	public static String isDate (String str) {
		if (str.isEmpty()) {
			//itt beletesszuk a response.datebe a mai datumot
			return "OK";
		}
		else {
			if (str.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
				return "OK";
				}
				
			else {
				return "NEMOK";
			}
				
			
//		    try {
//		    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//		        Date dateStr = (Date) formatter.parse(str);
//			    return "OK";
//
//		    } catch (ParseException e) {
//		    	return e.toString();
//		    }
//		    
		}
	}
	
	
	
	public static String[] validate (String[] InpLine, String[] respLine){
		
		for(int i = 0; i < InpLine.length ; i++) {
//			System.out.println("InpLine[" + i + "]" +InpLine[i]);
			switch (i) {
			case 0:  	respLine[0]= InpLine[i]	;							//LineNumber
						respLine[2]= "";
						
						break;
            case 1:  	respLine[2]= respLine[2] + "";									//LineNumber
            
            			break;
            case 2:  if (isInteger(InpLine[i]) != "OK") {				//OrderItemId
            				respLine[2]= respLine[2] + "OrderItemId not int or missing, ";
			            }
			            else {
			            	respLine[2]= respLine[2] + "";	
			            }
            
                     break;
            case 3:  if (InpLine[i].isEmpty()) {					//BuyerName	
            	
		            	respLine[2]= respLine[2] + "BuyerName missing, ";
							}
		            else {
		            	respLine[2]= respLine[2] + "";	
		            }
                     break;
//            case 12:  if (isInteger(InpLine[i]) != "OK") {				//OrderId
//						respLine[2]= respLine[2] + "OrderId not int or missing, ";
//			            
//					}
//            System.out.println("A: " + InpLine[i] + " b " + respLine[2]);
//         
//            break;
            case 4:  if (isEmail(InpLine[i]) != "OK") {					//BuyerEmail
            	 
						respLine[2]= respLine[2] + "BuyerEmail not valid or missing, ";
					}	
		            else {
		            	respLine[2]= respLine[2] + "";	
		            }

                     break;
            case 5:  if (InpLine[i].isEmpty()) {					//Address
            	
							respLine[2]= respLine[2] + "Address missing, ";
						}
		            else {
		            	
			            	respLine[2]= respLine[2] + "";
			            }
                     break;
            case 6:  if (isInteger(InpLine[i]) == "OK") {				//Postcode
							respLine[2]= respLine[2] + "";
						}
		            else {
		            	respLine[2]= respLine[2] + "Postcode not int or missing, ";	
	            	}
//            System.out.println(InpLine[i]);
                     break;
            case 7:  if (isValidPrice(InpLine[i], 1.00) == "Less than minimum price") {				//SalePrice
							respLine[2]= respLine[2] + "SalePrice less than minimum price, ";
						}
            		else if (isValidPrice(InpLine[i], 1.00) == "Not double") {
            			respLine[2]= respLine[2] + "SalePrice not double or missing, ";
            			}
            		else {
		            	respLine[2]= respLine[2] + "";	
	            	}
            		
                     break;
            case 8:  if (isValidPrice(InpLine[i], 0.00) == "Less than minimum price") {				//ShippingPrice
							respLine[2]= respLine[2] + "ShippingPrice less than minimum price, ";
						}
					else if (isValidPrice(InpLine[i], 0.00) == "Not double") {
						respLine[2]= respLine[2] + "ShippingPrice not double or missing, ";
						}
					else {
		            	respLine[2]= respLine[2] + "";	
	            	}
                     break;
            case 9: if ((InpLine[i]) == "") {					//SKU
							respLine[2]= respLine[2] + "SKU is missing, ";
						}	
		            else {
		            	respLine[2]= respLine[2] + "";	
		        	}									
                     break;
            case 10: if (isStatus(InpLine[i]) == "OK") {					//Status
							respLine[2]= respLine[2] + "Status is not valid or missing, ";
						}
		            else {
		            	respLine[2]= respLine[2] + "";	
		        	}
                     break;
            case 11: if (InpLine[i].isEmpty()) {
		            	LocalDate localDate = LocalDate.now();
		            	InpLine[i] = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate).toString();
            				}
            			if (isDate(InpLine[i]) != "OK") {					//OrderDate
							respLine[2]= respLine[2] + "OrderDate is not valid, ";
							
						}
			            else {
			            	respLine[2]= respLine[2] + "";	
			        	}
//	            		System.out.println("input: " + InpLine[i] + " response " + respLine[2]);
	                     break;
        }

		}
		
//		System.out.println("return" + Arrays.toString(respLine));
		return respLine;
		
	}

	public static void out (String[][] respl) throws IOException {
		FileWriter writer = new FileWriter("/home/zkny/eclipse-workspace/ReadFile/src/responseFile.csv");
		
		for (int i = 0; i < respl.length; i++) {
			for (int j = 0; j < 3; j++) {
				if (respl[i][2] == "") {
					respl[i][1]="OK";
					System.out.println(i);
				}
				 if (respl[i][1] !="OK") {
					respl[i][1]="ERROR";
				}
				 respl[0][1]="Status";
			    writer.append(respl[i][j]);
			    writer.append(";");
			    if (j == 2) {
			    	writer.append("\n");
			    }
			}
		}
		
		writer.close();
	}


}	
	